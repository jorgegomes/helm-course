{{- define "myproject.systemlabels" }}
  labels:
  {{- with .Values.tags }}
    first: {{ .machine }}
    second: {{ .rack }}
    third: {{ .drive }}
    name: {{ $.Release.Name }}
    app.kubernetes.io/instance: "{{ $.Release.Name }}"
    app.kubernetes.io/version: "{{ $.Chart.AppVersion }}"
    app.kubernetes.io/managed-by: "{{ $.Release.Service }}"
  {{- end }}
{{- end }}

{{- define "myproject.version" }}
app_name: {{ .Chart.Name }}
app_version: "{{ .Chart.Version }}"
{{- end }}